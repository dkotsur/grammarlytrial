#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
#
# Created on : 24.12.2019 $
# by : dkotsur $
#
# --- imports ----------------------------------------------------------------------------------------------------------
import re


REXP_NUMBER = r"^[-+]?[0-9]+([.,][0-9]+)*$"
REXP_QUANTITY = r"^[-+]?[0-9]+([.,][0-9]+)*\D{1,5}$"
REXP_PRICE1 = r"^[€£\$][0-9]+([.,][0-9]+)*$"
REXP_PRICE2 = r"^[0-9]+([.,][0-9])*[€£\$]$"
REXP_DASH = r"^[\-]+$"
REXP_OTHER = r"^\D*[-+]?[0-9]([.,][0-9])*\D*$"
REXP_WORD = r"[^a-zA-Z]*([a-zA-Z]+)[^a-zA-Z]*$"


FILTER1 = "\'\" \t\n‘`"
NUMBER_THRESHOLD = 100


def is_small_number(token):
    try:
        val = int(token)
    except ValueError as e:
        return False
    return val < NUMBER_THRESHOLD


def handle_token(token, lower=True):
    stoken = token.lower() if lower else token
    stoken = stoken.strip(FILTER1)
    if is_small_number(stoken):
        return stoken
    if re.match(REXP_PRICE1, stoken) or re.match(REXP_PRICE2, stoken):
        return "<pri>"
    elif re.match(REXP_QUANTITY, stoken):
        return "<qty>"
    elif re.match(REXP_NUMBER, stoken):
        return "<num>"
    elif re.match(REXP_OTHER, stoken):
        return "<oth>"
    elif re.match(REXP_DASH, stoken):
        return "-"
    else:
        match = re.match(REXP_WORD, stoken)
        if match is not None:
            return match.group(1)
    return stoken


def read_tokens(filename, encoding="utf8"):
    """Read tokens from file"""
    try:
        with open(filename, encoding=encoding) as file_input:
            lines = file_input.readlines()
            return map(str.split, lines), len(lines)
    except IOError:
        return [], 0


if __name__ == "__main__":

    tokens = [
        "ten",
        "--asda",
        "001231adsda",
        "12312",
        "99",
        "100$",
        "$0.001",
        "100m",
        "9kg",
    ]

    for token in tokens:
        print(token, "-", handle_token(token))
