#!/usr/bin/env python


def split(filename_data, filename_label, per=0.1, encoding='utf8'):
    with open(filename_data, "r", encoding=encoding) as fin:
        lines_data = fin.readlines()
    with open(filename_label, "r", encoding=encoding) as fin:
        lines_label = fin.readlines()
    cnt = int(len(lines_data) * per)
    with open(filename_data + ".p1", "w", encoding=encoding) as fout:
        fout.writelines(lines_data[:cnt])
    with open(filename_label + ".p1", "w", encoding=encoding) as fout:
        fout.writelines(lines_label[:cnt])
    with open(filename_data + ".p2", "w", encoding=encoding) as fout:
        fout.writelines(lines_data[cnt::])
    with open(filename_label + ".p2", "w", encoding=encoding) as fout:
        fout.writelines(lines_label[cnt::])


if __name__ == "__main__":
    split("../substituted-words/data/val.src",
          "../substituted-words/data/val.lbl")
