#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
#
# Created on : 24.12.2019 $
# by : dkotsur $
#
# --- imports ----------------------------------------------------------------------------------------------------------
from dataio.text import handle_token, read_tokens
from collections import defaultdict
from tqdm import tqdm


class Vocab(object):

    UNKNOWN_TOKEN = "<unk>"

    def __init__(self, lower=True):
        """Constructor of the method

        Args:
             lower: Boolean, if True convert words to lower case
        """
        self.lower = lower
        self.word_to_index = {
            Vocab.UNKNOWN_TOKEN: 1
        }
        self.index_to_word = {
            1: Vocab.UNKNOWN_TOKEN
        }
        self.max_index = 2

    def texts_to_sequences(self, texts):
        """Converts texts to sequences of integers

        Args:
            texts: list of lists of strings, where each string represents a word

        Returns:
            list of lists of integers
        """
        sequences = []
        for text in texts:
            sequence = []
            for word in text:
                hword = handle_token(word, self.lower)
                if hword in self.word_to_index:
                    sequences.append(self.word_to_index[hword])
                else:
                    sequences.append(self.word_to_index[Vocab.UNKNOWN_TOKEN])
            sequences.append(sequence)
        return sequences

    def save(self, filename, encoding="utf8"):
        """Save vocabulary to file

        Args:
            filename: String of output file path
            encoding: String of file charset encoding

        Returns:
            True, if vocabulary is saved successfully and False, otherwise
        """
        try:
            with open(filename, "w", encoding=encoding) as file_output:
                file_output.write("\n".join(self.word_to_index.keys()))
        except IOError:
            return False
        return True

    def __getitem__(self, item):
        if type(item) == int:
            return self.index_to_word.get(item, None)
        if type(item) == str:
            if item in self.word_to_index:
                return self.word_to_index[item]
            else:
                return self.word_to_index[Vocab.UNKNOWN_TOKEN]

    def __len__(self):
        return self.max_index

    @staticmethod
    def create(filenames, lower=True, min_occurrences=5, encoding="utf8"):
        """Create vocabulary from corpus texts

        Args:
            filenames: list of Strings, each is a path to a text file
            lower: Boolean, if True all tokens are transformed to lowercase
            min_occurrences: Integer, the minimum number of occurrences of a word to add it word to vocabulary

        Returns:
            Vocab object
        """
        unique_tokens = defaultdict(int)
        for filename in filenames:
            data, size = read_tokens(filename, encoding)
            for sequence in tqdm(data, total=size, desc=filename):
                for token in sequence:
                    h_token = handle_token(token, lower=lower)
                    unique_tokens[h_token] += 1

        vocab = Vocab()
        for token, cnt in unique_tokens.items():
            if cnt >= min_occurrences:
                vocab.word_to_index[token] = vocab.max_index
                vocab.index_to_word[vocab.max_index] = token
                vocab.max_index += 1
        return vocab

    @staticmethod
    def load(filename, encoding="utf8"):
        """Loads vocab from the file

        Args:
            filename: String path to the file

        Returns:
            Vocab object
        """
        try:
            with open(filename, encoding=encoding) as file_input:
                words = map(str.strip, file_input.readlines())
                vocab = Vocab()
                for i, word in enumerate(words):
                    vocab.word_to_index[word] = i+1
                    vocab.index_to_word[i+1] = word
                vocab.max_index = len(vocab.word_to_index) + 1
                return vocab
        except IOError:
            return None
