#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
#
# Created on : 24.12.2019 $
# by : dkotsur $
#
# --- imports ----------------------------------------------------------------------------------------------------------
import json
from collections import namedtuple


def load_config(filename, encoding="utf8"):
    """Load configuration from JSON file"""
    try:
        with open(filename, "r", encoding=encoding) as file_in:
            return json.load(file_in, object_hook=lambda d: namedtuple('Entry', d.keys())(*d.values()))
    except IOError:
        return None
