#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
#
# Created on : 25.12.2019 $
# by : dkotsur $
#
# --- imports ----------------------------------------------------------------------------------------------------------
import numpy as np
from tqdm import tqdm
from tensorflow import keras
from tensorflow.keras.preprocessing.sequence import pad_sequences

import multiprocessing as mp
from dataio.text import handle_token, read_tokens
from dataio.vocab import Vocab

_vocab = None
_lower = False


def mp_init_proc(vocab_filename, lower):
    """Initialize vocabulary for a process"""
    global _vocab, _lower
    _vocab = Vocab.load(vocab_filename)
    _lower = lower


def mp_handle_sequence(sequence):
    """Handle one input sequence (sentence)"""
    global _vocab, _lower  # get global variables for the process
    # compute output
    output = []
    for token in sequence:
        htoken = handle_token(token, _lower)
        output.append(_vocab[htoken])
    return output


class TextDataset(object):
    """Load text data from files and prepare sequence data

    Args:
        vocab: Vocab object
        text_filenames: list[String] contains paths to text files
        label_filenames: list[String] contains paths to label files
        lower: Boolean, if True all words are transformed to lower case
        workers: Integer, the number of processes to handle the data
    """

    def __init__(self, vocab_filename, text_filenames, label_filenames, lower=True, workers=1, validate=True):

        # initialize attributes
        self.lower = lower
        self.vocab_filename = vocab_filename
        self.text_filenames = text_filenames
        self.label_filenames = label_filenames
        self.data = []
        self.label = []

        # initialize pool
        self.pool = mp.Pool(workers, initializer=mp_init_proc, initargs=(vocab_filename, lower))

        # read data
        for text_filename, label_filename in zip(text_filenames, label_filenames):
            n = self.__load_text(text_filename)
            if label_filename is not None:  # if there's a label file, load it
                assert self.__load_labels(label_filename) == n
            else:  # otherwise assume all labels to be 0s
                self.__extend_labels_last(n)
        if validate:
            assert len(self.label) == len(self.data)
            for sequence, label in zip(self.data, self.label):
                assert(len(sequence) == len(label))
        self.pool.close()

    def __getitem__(self, i):
        return self.data[i], self.label[i]

    def __len__(self):
        return len(self.data)

    def __load_text(self, filename):
        # read tokens
        token_sequences, n = read_tokens(filename)
        # preprocess tokens
        sequences = tqdm(self.pool.imap(mp_handle_sequence, token_sequences), total=n, desc=filename)
        self.data.extend(sequences)
        # return number of sequences
        return len(sequences)

    def __load_labels(self, filename, encoding="utf8"):
        try:
            with open(filename, encoding=encoding) as file_input:
                lines = file_input.readlines()
                for line in lines:
                    self.label.append(np.asarray(line.split(), dtype=np.int))
                return len(lines)
        except IOError:
            return []

    def __extend_labels_last(self, n):
        for idx in range(-n, 0):
            self.label.append(np.array([0] * len(self.data[idx]), dtype=np.int))


class SequenceDataLoader(keras.utils.Sequence):
    """Load data from dataset and make batches

    Args:
        dataset: instance of Dataset class for data loading and preprocessing.
        batch_size: Integer number of images in batch.
        shuffle: Boolean, if `True` shuffle data indexes each epoch.
        pad: Boolean, if `True` apply sequence padding with zeros
        pad_mode: if `post` add zeros at the end, if `pre` add zeros at the beginning
    """

    def __init__(self, dataset, batch_size=1, shuffle=True, pad=True, pad_mode="post", max_len=10):
        self.dataset = dataset
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.pad = pad
        self.pad_mode = pad_mode
        self.max_len = max_len
        self.indexes = np.arange(len(dataset))
        self.on_epoch_end()

    def __getitem__(self, i):
        """Prepares and returns batch, applies padding if needed"""
        # collect batch data
        start = i * self.batch_size
        stop = (i + 1) * self.batch_size
        batch_data, batch_labels = [], []
        for j in range(start, stop):
            data, label = self.dataset[self.indexes[j]]
            batch_data.append(data)
            batch_labels.append(label)
        # apply padding
        if self.pad:
            batch_data = pad_sequences(batch_data, padding=self.pad_mode, maxlen=self.max_len, truncating="post")
            batch_labels = pad_sequences(batch_labels, padding=self.pad_mode, maxlen=self.max_len, truncating="post")
        # return batch
        return np.asarray(batch_data), np.asarray(batch_labels)

    def __len__(self):
        """Denotes the number of batches per epoch"""
        return len(self.indexes) // self.batch_size

    def on_epoch_end(self):
        """Callback function to shuffle indexes each epoch"""
        if self.shuffle:
            self.indexes = np.random.permutation(self.indexes)
