import os
import re
import numpy as np
from dataio.config import load_config
from dataio.dataset import TextDataset, SequenceDataLoader
from dataio.vocab import Vocab
from model import blstm
from glob import glob


def find_latest_weights(config):
    """Get weights filename for the latest epoch"""

    regex_pattern = r"model-(\d+)+.ckpt"
    checkpoints_dir = os.path.join(config.checkpoints, config.name + "__ex" + str(config.experiment_id))

    epochs = dict()
    for filename in glob(os.path.join(checkpoints_dir, "model-*.ckpt.data*")):
        match = re.search(regex_pattern, os.path.basename(filename))
        epochs[int(match.group(1))] = match.group(0)

    return os.path.join(checkpoints_dir, epochs[max(epochs.keys())])


def predict(config_filename, predict_filename):
    # load configuration file
    config = load_config(config_filename)

    # find filename of the latest weights
    weights_path = find_latest_weights(config)

    # load vocab
    vocab = Vocab.load(config.vocab.path)

    # load dataset
    predict_dataset = TextDataset(config.vocab.path, [predict_filename], [None])

    # create loader
    predict_loader = SequenceDataLoader(predict_dataset, batch_size=1, shuffle=False, max_len=config.dataset.max_len)

    # create a neural network
    model = blstm.make_model(vocab_size=len(vocab)+1,
                             embedding_size=config.network.embedding_size,
                             units_lstm=config.network.units_lstm,
                             units_dense=config.network.units_dense,
                             dropout=config.network.dropout)

    print("Loading weights from:", weights_path)
    model.load_weights(weights_path)

    predictions = []
    for batch in predict_loader:
        pred = model.predict_on_batch(batch[0])
        predictions.append(np.squeeze(pred.numpy()))

        print(np.asarray((predictions[-1] > 0.5), dtype=np.float))


if __name__ == "__main__":
    predict("./configs/config_blstm_trial_v2.json",
            "./substituted-words/data/val.src.p1")