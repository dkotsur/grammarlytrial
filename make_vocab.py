#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
#
# Created on : 25.12.2019 $
# by : dkotsur $
#
# --- imports ----------------------------------------------------------------------------------------------------------
from dataio.vocab import Vocab


if __name__ == "__main__":
    vocab = Vocab.create([
        "./substituted-words/data/test.src",
        "./substituted-words/data/train.src",
        "./substituted-words/data/train.tgt",
        "./substituted-words/data/val.src",
        "./substituted-words/data/val.tgt"
    ], min_occurrences=20)

    vocab.save("./data/vocab_tiny.txt")