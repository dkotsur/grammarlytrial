#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
#
# Created on : 25.12.2019 $
# by : dkotsur $
#
# --- imports ----------------------------------------------------------------------------------------------------------
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import LSTM, Dense, Embedding, TimeDistributed, Dropout, Bidirectional, Input
from tensorflow.keras.metrics import sparse_categorical_accuracy


def make_model(vocab_size,
               embedding_size,
               sequence_len=None,
               units_lstm=[100, 100],
               units_dense=[50],
               dropout=0.1):

    model = Sequential()
    model.add(Embedding(input_dim=vocab_size, output_dim=embedding_size, input_length=sequence_len))
    for units in units_lstm:
        model.add(Bidirectional(LSTM(units=units, return_sequences=True, recurrent_dropout=dropout)))

    for units in units_dense:
        model.add(TimeDistributed(Dense(units, activation="relu")))
        model.add(Dropout(dropout))

    model.add(TimeDistributed(Dense(2, activation="softmax")))
    model.compile(optimizer="adam", loss="sparse_categorical_crossentropy", metrics=[sparse_categorical_accuracy])

    return model
