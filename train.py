#!/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------------
#
# Created on : 25.12.2019 $
# by : dkotsur $
#
# --- imports ----------------------------------------------------------------------------------------------------------
import os
from dataio.dataset import TextDataset, SequenceDataLoader
from dataio.vocab import Vocab
from dataio.config import load_config
from model import blstm

from tensorflow.keras.callbacks import ModelCheckpoint, CSVLogger, TensorBoard, ReduceLROnPlateau, EarlyStopping



def train(config_filename):

    # load configuration file
    config = load_config(config_filename)

    # load vocabulary
    vocab = Vocab.load(config.vocab.path)

    # load training dataset
    train_dataset = TextDataset(config.vocab.path,
                                config.dataset.train.x,
                                config.dataset.train.y,
                                workers=4)

    # load validation dataset
    valid_dataset = TextDataset(config.vocab.path,
                                config.dataset.val.x,
                                config.dataset.val.y)

    # print information
    print("Training dataset size:", len(train_dataset))
    print("Validation dataset size:", len(valid_dataset))

    # create a data loader for training dataset
    train_loader = SequenceDataLoader(train_dataset, batch_size=config.train.batch_size, max_len=config.dataset.max_len)

    # create a data loader for validation dataset
    valid_loader = SequenceDataLoader(valid_dataset, batch_size=config.train.batch_size, shuffle=False, max_len=config.dataset.max_len)

    # create a neural network
    model = blstm.make_model(vocab_size=len(vocab)+1,
                             embedding_size=config.network.embedding_size,
                             units_lstm=config.network.units_lstm,
                             units_dense=config.network.units_dense,
                             dropout=config.network.dropout)

    # information about the model
    print(model.summary())

    experiment_name = config.name + "__ex" + str(config.experiment_id)
    checkpoints_dir = os.path.join(config.checkpoints, experiment_name)

    try:
        os.mkdir(checkpoints_dir)
    except IOError:
        print("Folder %s exist." % checkpoints_dir)

    callbacks = [
        # save model checkpoints
        ModelCheckpoint(os.path.join(checkpoints_dir, 'model-{epoch:03d}.ckpt'),
                        save_weights_only=True,
                        save_best_only=True,
                        mode='auto',
                        verbose=1),

        # reduce learning rate on plateau
        ReduceLROnPlateau(),

        # stop training if no progress
        EarlyStopping(min_delta=0.0001,
                      patience=config.train.epochs / 10),

        # dump information to tensorboard
        TensorBoard(log_dir=os.path.join(config.logs, experiment_name))
    ]

    # train model
    model.fit_generator(train_loader,
                        steps_per_epoch=len(train_loader),
                        epochs=config.train.epochs,
                        callbacks=callbacks,
                        validation_data=valid_loader,
                        validation_steps=len(valid_loader),
                        verbose=config.train.verbose,
                        workers=config.train.workers,
                        max_queue_size=config.train.max_queue_size,
                        class_weight=[0.01, 0.99])

if __name__ == "__main__":
    train("./configs/config_blstm_trial_v2.json")
